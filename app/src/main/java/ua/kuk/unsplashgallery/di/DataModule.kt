package ua.kuk.unsplashgallery.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import ua.kuk.unsplashgallery.data.DownloadImagesListRepositoryImpl
import ua.kuk.unsplashgallery.data.network.ApiService
import ua.kuk.unsplashgallery.data.network.RetrofitInstance
import ua.kuk.unsplashgallery.domain.DownloadImagesListRepository

@Module
interface DataModule {

    @ApplicationScope
    @Binds
    fun bindConnectToApiRepository(impl: DownloadImagesListRepositoryImpl): DownloadImagesListRepository

    companion object {
        @Provides
        @ApplicationScope
        fun provideApiService(): ApiService {
            return RetrofitInstance.connectToApiService
        }
    }
}