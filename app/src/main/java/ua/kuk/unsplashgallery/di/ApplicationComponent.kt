package ua.kuk.unsplashgallery.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import ua.kuk.unsplashgallery.UnsplashGalleryApplication
import ua.kuk.unsplashgallery.presentation.fragments.imageslist.ImagesListFragment

@ApplicationScope
@Component(modules = [DataModule::class, ViewModelModule::class])
interface ApplicationComponent {

    fun inject(application: UnsplashGalleryApplication)
    fun inject(imagesListFragment: ImagesListFragment)

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance application: Application
        ): ApplicationComponent
    }
}