package ua.kuk.unsplashgallery.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ua.kuk.unsplashgallery.presentation.fragments.imageslist.ImagesListViewModel

@Module
interface ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ImagesListViewModel::class)
    fun bindInitializeViewModel(viewModel: ImagesListViewModel): ViewModel
}
