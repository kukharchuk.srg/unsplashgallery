package ua.kuk.unsplashgallery.presentation.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import ua.kuk.unsplashgallery.R
import ua.kuk.unsplashgallery.databinding.FragmentFullImageBinding

class FullImageFragment : Fragment(R.layout.fragment_full_image) {
    private val binding: FragmentFullImageBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setImage()
    }

    private fun setImage() {
        val imageUrl = requireArguments().getString(EXTRA_IMAGE_URL)
        Glide.with(requireContext()).load(imageUrl).into(binding.ivFullFragment)
    }

    companion object {
        private const val EXTRA_IMAGE_URL = "full_size_image_url"

        fun newInstance(fullImageUrl: String): FullImageFragment {
            return FullImageFragment().apply {
                arguments = Bundle().apply {
                    putString(EXTRA_IMAGE_URL, fullImageUrl)
                }
            }
        }
    }
}