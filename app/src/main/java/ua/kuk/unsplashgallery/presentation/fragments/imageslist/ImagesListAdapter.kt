package ua.kuk.unsplashgallery.presentation.fragments.imageslist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import ua.kuk.unsplashgallery.databinding.ItemImageBinding
import ua.kuk.unsplashgallery.domain.ImageEntity

class ImagesListAdapter : ListAdapter<ImageEntity, ImageItemViewHolder>(ImageItemDiffCalBack) {

    var onImageClick: ((item: ImageEntity) -> Unit)? = null
    var onReachedEnd: (() -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageItemViewHolder {
        val binding = ItemImageBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ImageItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageItemViewHolder, position: Int) {
        val item = getItem(position)
        holder.setData(item, onImageClick)
        if (position == currentList.size - 3) {
            onReachedEnd?.invoke()
        }
    }
}