package ua.kuk.unsplashgallery.presentation.fragments.imageslist

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import ua.kuk.unsplashgallery.R
import ua.kuk.unsplashgallery.UnsplashGalleryApplication
import ua.kuk.unsplashgallery.databinding.FragmentImagesListBinding
import ua.kuk.unsplashgallery.presentation.fragments.FullImageFragment
import javax.inject.Inject


class ImagesListFragment : Fragment(R.layout.fragment_images_list) {

    private val component by lazy {
        (requireActivity().application as UnsplashGalleryApplication).component
    }

    private val binding: FragmentImagesListBinding by viewBinding()
    private val adapter by lazy {
        ImagesListAdapter()
    }

    @Inject
    lateinit var factory: ViewModelFactory
    private val viewModel: ImagesListViewModel by viewModels {
        factory
    }

    override fun onAttach(context: Context) {
        component.inject(this)
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setRecyclerView()
        setStates()
    }

    private fun setStates() {
        lifecycleScope.launch {
            viewModel.state.collectLatest {
                when (it) {
                    is Initial -> {
                        // nothing use to be here
                    }
                    is Error -> {
                        Toast.makeText(
                            requireContext(),
                            getString(R.string.message_error),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    is Success -> {
                        adapter.submitList(it.list)
                        adapter.onReachedEnd = {
                            viewModel.downLoadImages()
                        }
                    }
                }
            }
        }
    }

    private fun setRecyclerView() {
        binding.rvImagesList.setHasFixedSize(true)
        binding.rvImagesList.adapter = adapter
        adapter.onImageClick = {
            requireActivity().supportFragmentManager.beginTransaction().replace(
                R.id.fragmentContainerViewMain,
                FullImageFragment.newInstance(it.fullSizeImageUrl)
            ).addToBackStack(null)
                .commit()
        }
    }
}