package ua.kuk.unsplashgallery.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ua.kuk.unsplashgallery.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}