package ua.kuk.unsplashgallery.presentation.fragments.imageslist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import ua.kuk.unsplashgallery.domain.DownloadImagesListRepositoryUseCase
import ua.kuk.unsplashgallery.domain.ImageEntity
import javax.inject.Inject

class ImagesListViewModel @Inject constructor(
    private val downloadImagesListRepositoryUseCase: DownloadImagesListRepositoryUseCase
) : ViewModel() {
    private val _state = MutableStateFlow<AppInitializeState>(Initial)
    val state = _state.asStateFlow()

    private var tempListOfImages = listOf<ImageEntity>()
    private var pageNumber = 1

    init {
        downLoadImages()
    }

    fun downLoadImages() {
        viewModelScope.launch {
            try {
                val result =
                    downloadImagesListRepositoryUseCase.invoke(pageNumber = pageNumber.toString())
                val temp = tempListOfImages.toMutableList().plus(result)
                tempListOfImages = temp
                pageNumber++
                _state.value = Success(temp)
            } catch (e: Exception) {
                _state.value = Error
                _state.value = Initial
            }
        }
    }
}

sealed interface AppInitializeState
object Initial : AppInitializeState
object Error : AppInitializeState
data class Success(val list: List<ImageEntity>) : AppInitializeState