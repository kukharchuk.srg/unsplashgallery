package ua.kuk.unsplashgallery.presentation.fragments.imageslist

import androidx.recyclerview.widget.DiffUtil
import ua.kuk.unsplashgallery.domain.ImageEntity

object ImageItemDiffCalBack : DiffUtil.ItemCallback<ImageEntity>() {

    override fun areItemsTheSame(oldItem: ImageEntity, newItem: ImageEntity): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: ImageEntity, newItem: ImageEntity): Boolean {
        return oldItem == newItem
    }
}