package ua.kuk.unsplashgallery.presentation.fragments.imageslist

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ua.kuk.unsplashgallery.databinding.ItemImageBinding
import ua.kuk.unsplashgallery.domain.ImageEntity

class ImageItemViewHolder(private val binding: ItemImageBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun setData(imageItem: ImageEntity, onImageClick: ((item: ImageEntity) -> Unit)?) {
        loadImage(imageItem)
        binding.tvAuthorImageItem.text = imageItem.userName
        val description = imageItem.description.ifBlank {
            imageItem.reserveDescription
        }
        binding.tvDescriptionImageItem.text = description

        binding.root.setOnClickListener {
            onImageClick?.invoke(imageItem)
        }
    }

    private fun loadImage(imageItem: ImageEntity) {
        Glide.with(binding.root.context)
            .load(imageItem.regularImageUrl)
            .into(binding.ivImageItem)
    }
}