package ua.kuk.unsplashgallery

import android.app.Application
import ua.kuk.unsplashgallery.di.DaggerApplicationComponent

class UnsplashGalleryApplication : Application() {

    val component by lazy {
        DaggerApplicationComponent.factory().create(this)
    }

    override fun onCreate() {
        component.inject(this)
        super.onCreate()
    }
}