package ua.kuk.unsplashgallery.domain

import javax.inject.Inject

class DownloadImagesListRepositoryUseCase @Inject constructor(
    private val downloadImagesListRepository: DownloadImagesListRepository
) {

    suspend operator fun invoke(pageNumber: String) =
        downloadImagesListRepository.downloadImageList(pageNumber)

}