package ua.kuk.unsplashgallery.domain

data class ImageEntity(
    val id: String,
    val description: String,
    val reserveDescription: String,
    val fullSizeImageUrl: String,
    val regularImageUrl: String,
    val smallImageSizeUrl: String,
    val userId: String,
    val userName: String
)