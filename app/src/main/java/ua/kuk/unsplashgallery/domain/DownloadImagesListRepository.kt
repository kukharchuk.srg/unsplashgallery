package ua.kuk.unsplashgallery.domain

interface DownloadImagesListRepository {

    suspend fun downloadImageList(pageNumber: String): List<ImageEntity>
}