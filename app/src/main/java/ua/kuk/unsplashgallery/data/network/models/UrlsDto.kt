package ua.kuk.unsplashgallery.data.network.models

import com.google.gson.annotations.SerializedName

data class UrlsDto(
    @SerializedName("full")
    val full: String? = null,

    @SerializedName("regular")
    val regular: String? = null,

    @SerializedName("small")
    val small: String? = null,
)
