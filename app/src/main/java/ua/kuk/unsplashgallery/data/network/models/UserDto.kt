package ua.kuk.unsplashgallery.data.network.models

import com.google.gson.annotations.SerializedName

data class UserDto(
    @SerializedName("id")
    val id: String? = null,

    @SerializedName("name")
    val name: String? = null
)