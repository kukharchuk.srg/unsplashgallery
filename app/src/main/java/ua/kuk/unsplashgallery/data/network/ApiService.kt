package ua.kuk.unsplashgallery.data.network

import retrofit2.http.GET
import retrofit2.http.Query
import ua.kuk.unsplashgallery.BuildConfig
import ua.kuk.unsplashgallery.data.network.models.ImageInformationDto

interface ApiService {

    @GET("photos")
    suspend fun getListOfPhotos(
        @Query(PARAM_CLIENT_ID) id: String = BuildConfig.UNSPLASH_API_KEY,
        @Query(PARAM_PAGE_NUMBER) pageNumber: String,
        @Query(PARAM_PHOTOS_PER_PAGE) itemsPerPage: String = PHOTOS_PER_PAGE,
    ): List<ImageInformationDto>

    companion object {
        private const val PARAM_CLIENT_ID = "client_id"
        private const val PARAM_PAGE_NUMBER = "page"
        private const val PARAM_PHOTOS_PER_PAGE = "per_page"
        private const val PHOTOS_PER_PAGE = "21"
    }
}