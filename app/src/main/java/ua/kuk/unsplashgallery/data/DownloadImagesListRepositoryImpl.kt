package ua.kuk.unsplashgallery.data

import ua.kuk.unsplashgallery.data.network.ApiService
import ua.kuk.unsplashgallery.domain.DownloadImagesListRepository
import ua.kuk.unsplashgallery.domain.ImageEntity
import javax.inject.Inject

class DownloadImagesListRepositoryImpl @Inject constructor(
    private val mapper: Mapper,
    private val apiService: ApiService
) : DownloadImagesListRepository {

    override suspend fun downloadImageList(pageNumber: String): List<ImageEntity> {
        val response = apiService.getListOfPhotos(pageNumber = pageNumber)
        val mappedResponse = response.map {
            mapper.dtoResponseToImageEntity(it)
        }
        return mappedResponse
    }
}