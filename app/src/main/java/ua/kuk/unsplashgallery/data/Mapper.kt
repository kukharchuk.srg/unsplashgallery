package ua.kuk.unsplashgallery.data

import ua.kuk.unsplashgallery.data.network.models.ImageInformationDto
import ua.kuk.unsplashgallery.domain.ImageEntity
import javax.inject.Inject

class Mapper @Inject constructor() {

    fun dtoResponseToImageEntity(imageInformationDto: ImageInformationDto): ImageEntity {
        val id = imageInformationDto.id ?: ""
        val description = imageInformationDto.description ?: ""
        val reserveDescription = imageInformationDto.altDescription ?: ""
        val fullSizeImageUrl = imageInformationDto.urls?.full ?: ""
        val regularImageUrl = imageInformationDto.urls?.regular ?: ""
        val smallImageSizeUrl = imageInformationDto.urls?.small ?: ""
        val userId = imageInformationDto.user?.id ?: ""
        val userName = imageInformationDto.user?.name ?: ""
        return ImageEntity(
            id,
            description,
            reserveDescription,
            fullSizeImageUrl,
            regularImageUrl,
            smallImageSizeUrl,
            userId,
            userName
        )
    }
}