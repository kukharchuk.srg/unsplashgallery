package ua.kuk.unsplashgallery.data.network.models

import com.google.gson.annotations.SerializedName

data class ImageInformationDto(
    @SerializedName("id")
    val id: String? = null,

    @SerializedName("description")
    val description: String? = null,

    @SerializedName("alt_description")
    val altDescription: String? = null,

    @SerializedName("urls")
    val urls: UrlsDto? = UrlsDto(),

    @SerializedName("user")
    val user: UserDto? = UserDto()
)