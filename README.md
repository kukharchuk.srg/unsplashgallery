# UnsplashGallery

## You can find debug APK file in [debug_apk](https://gitlab.com/kukharchuk.srg/unsplashgallery/-/tree/main/debug_apk) directory to test app on your  device

## Project Flow
- Kotlin
- MVVM
- Network calls through REST API + OkHttp
- Coroutines, Flow
- Dependency injection by Dagger2
- ViewBinding delegates
- Navigation through FragmentManager
- Glide
- ...

## In any case, I will be happy to answer your questions





